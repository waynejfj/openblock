

class PseudoNativeObjectAgent extends NativeObjectAgent {
    constructor(var_args) {
        super(...arguments);
        this.__init();
    }
    static $__initReturned(self, pv) {
        if (pv && pv.__init && !pv.__inited) {
            pv.__init();
            return pv;
        } else if (typeof pv == 'function') {
            return function () {
                let r = pv.apply(self, arguments);
                if (r && r.__init && !r.__inited) {
                    PseudoNativeObjectAgent.$__initReturned(self, r);
                }
                return r;
            }
        }
        return pv;
    }
    static $__init(self) {
        self.__inited = true;
        let props = self.__propList();
        for (let p of props) {
            Object.defineProperty(self, p, {
                get() {
                    let pv = self.__get(p);
                    return PseudoNativeObjectAgent.$__initReturned(self, pv);
                },
                set(newValue) {
                    self.__set(p, newValue);
                },
                enumerable: true,
                configurable: true,
            });
        };
    }
    __init() {
        PseudoNativeObjectAgent.$__init(this);
    }
}
PseudoNativeObjectAgent.$__init(console);
PseudoNativeObjectAgent.$__init(util);


function base64ToArrayBuffer(base64) {
    // 解码Base64字符串  
    var binary = util.atob(base64);

    // 创建一个Uint8Array，其中包含我们的二进制数据  
    var arrayBuffer = new Uint8Array(binary.length);

    // 将每个字符转换为对应的字节  
    for (var i = 0; i < binary.length; i++) {
        // if (arrayBuffer.__debugger) {
        //     arrayBuffer.__set(i, binary.charCodeAt(i));
        // } else {
        //     arrayBuffer[i] = binary.charCodeAt(i);
        // }
        arrayBuffer[i] = binary.charCodeAt(i);
    }
    return arrayBuffer.buffer;
    // return arrayBuffer.__debugger ? arrayBuffer.getBuffer() : arrayBuffer.buffer;
}
var loadedScript;


function loadScript(base64OfBytecode) {
    console.log('starting loading script...');
    console.log(nativeLibs);
    var bytecode = base64ToArrayBuffer(base64OfBytecode);
    var script = ScriptLoader.loadScript(bytecode, nativeLibs);
    loadedScript = script;
    console.log("loaded script", loadScript.entryFSMName);
}

function EventTarget() { }

// 添加事件监听器
EventTarget.prototype.addEventListener = function (type, listener) {
    if (this._listeners === undefined) this._listeners = {};
    if (!this._listeners[type]) this._listeners[type] = [];
    this._listeners[type].push(listener);

    // 对于 IE8 及以下版本
    if (window.attachEvent) {
        this['on' + type] = listener;
        this.attachEvent('on' + type, listener); // IE 特有的事件绑定方式
    } else {
        this.addEventListener = function (type, listener) { // 其他现代浏览器
            this.addEventListener(type, listener, false);
        };
        this.addEventListener(type, listener, false);
    }
};

// 移除事件监听器
EventTarget.prototype.removeEventListener = function (type, listener) {
    if (this._listeners === undefined || !this._listeners[type]) return;
    var index = this._listeners[type].indexOf(listener);
    if (index > -1) {
        this._listeners[type].splice(index, 1);

        // 对于 IE8 及以下版本
        if (window.detachEvent) {
            this.detachEvent('on' + type, listener); // IE 特有的事件解绑方式
        } else {
            this.removeEventListener(type, listener, false);
        }
    }
};

// 触发事件
EventTarget.prototype.dispatchEvent = function (event) {
    if (typeof event === 'string') {
        event = new CustomEvent(event); // 假设 CustomEvent 构造函数可用，若不支持则需自行创建事件对象
    }
    if (!event.target) event.target = this;

    if (this._listeners && this._listeners[event.type]) {
        for (var i = 0; i < this._listeners[event.type].length; i++) {
            this._listeners[event.type][i].call(this, event);
        }
    }

    // IE 不支持 dispatchEvent，需要手动触发
    if (window.document.createEventObject) {
        var ieEvent = document.createEventObject();
        this.fireEvent('on' + event.type, ieEvent); // IE 特有的事件触发方式
    } else {
        this.dispatchEvent(event); // 现代浏览器直接调用
    }
};
